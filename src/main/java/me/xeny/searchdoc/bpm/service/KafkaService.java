package me.xeny.searchdoc.bpm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class KafkaService {

    @Setter
    @Autowired
    private ObjectMapper objectMapper;

    @KafkaListener(id = "bpm", topics = {"filestore"}, containerFactory = "singleFactory")
    public void consume(UUID fileId) {
        log.info("=> consumed {}", fileId.toString());
    }

}
