package me.xeny.searchdoc.bpm.bpm.task;

import lombok.RequiredArgsConstructor;
import me.xeny.searchdoc.messaging.Message;
import me.xeny.searchdoc.messaging.impl.PrintCommand;
import me.xeny.searchdoc.messaging.impl.PrintPayload;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AnalyzeTask implements JavaDelegate {
    private final KafkaTemplate<UUID, Message> kafkaTemplate;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        System.out.println("AnalyzeTask push!");
        PrintCommand printCommand = new PrintCommand(new PrintPayload("Print"));
        kafkaTemplate.send(printCommand.getTopic(), UUID.randomUUID(), printCommand);
    }
}
